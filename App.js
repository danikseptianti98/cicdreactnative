import React, {Component} from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
  TouchableOpacity,
} from 'react-native';
import codePush from 'react-native-code-push';

// let codePushOptions = {
//   checkFrequency: codePush.CheckFrequency.ON_APP_RESUME,
// };
let codePushOptions = {
  checkFrequency: codePush.CheckFrequency.ON_APP_RESUME,
  installMode: codePush.InstallMode.ON_NEXT_RESUME,
};

class App extends Component {
  componentDidMount() {
    codePush.sync({
      updateDialog: true,
      installMode: codePush.InstallMode.ON_NEXT_RESUME,
    });
    console.log('test', codePush.InstallMode.ON_NEXT_RESUME);
  }

  render() {
    return (
      <View style={{flex: 1, padding: 40, marginTop: 30}}>
        <TouchableOpacity onPress={this.onButtonPress}>
          <Text>Wah kalau gabisa parah sih</Text>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
  },
  highlight: {
    fontWeight: '700',
  },
});

export default App;
App = codePush(codePushOptions)(App);
